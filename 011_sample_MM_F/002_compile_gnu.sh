#!/bin/bash

	# 1. compile test

	g++  -static                    \
	     -isystem 			       \
          /opt/gtest/170/include     \
          sample_11.cpp              \
          sample_11_unittest.cpp     \
	     driver_program.cpp         \
      	-pthread 			       \
          /opt/gtest/170/libgtest.a  \
          -o x_gnu

