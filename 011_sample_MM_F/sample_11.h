//====================================//
// A sample program demonstrating     //
// using Google C++ testing framework //
//====================================//

#ifndef GTEST_SAMPLE_11_H
#define GTEST_SAMPLE_11_H

// factorial

long double Factorial(long double n);

// returns true if n is a prime number

bool IsPrime(long int n);

#endif  // GTEST_SAMPLE_11_H
