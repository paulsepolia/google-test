//====================================//
// A sample program demonstrating 	   //
// using Google C++ testing framework //
//====================================//

#include "sample_11.h"

// factorial

long double Factorial(long double n)
{
     long double result = static_cast<long double>(1.0);

     for (long int i = 1; i <= static_cast<long int>(n); i++) {
          result *= i;
     }

     return result;
}

// Returns true if n is a prime number

bool IsPrime(long int n)
{

     // Trivial case 1: small numbers

     if (n <= 1L) return false;

     // Trivial case 2: even numbers

     if (n % 2L == 0) return n == 2L;

     // Now, we have that n is odd and n >= 3

     // Try to divide n by every odd number i, starting from 3

     for (int i = 3L; ; i += 2L) {

          // We only have to try i up to the squre root of n

          if (i > n/i) break;

          // Now, we have i <= n/i < n
          // If n is divisible by i, n is not prime

          if (n % i == 0L) return false;
     }

     // n has no integer factor in the range (1, n), and thus is prime

     return true;
}

// END
