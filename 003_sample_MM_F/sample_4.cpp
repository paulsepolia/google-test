// A sample program demonstrating using Google C++ testing framework.

#include <stdio.h>

#include "sample_4.h"

// Returns the current counter value, and increments it.
int Counter::Increment()
{
     return counter_++;
}

// Prints the current counter value to STDOUT.
void Counter::Print() const
{
     printf("%d", counter_);
}
