
#include "gtest/gtest.h"
#include "sample_4.h"

// Tests the Increment() method.
TEST(Counter, Increment)
{
     Counter c;

     // EXPECT_EQ() evaluates its arguments exactly once, so they
     // can have side effects.

     EXPECT_EQ(0, c.Increment());
     EXPECT_EQ(1, c.Increment());
     EXPECT_EQ(2, c.Increment());
}

// the main

int main(int argc, char **argv)
{
     ::testing::InitGoogleTest(&argc, argv);

     int res = RUN_ALL_TESTS();

     return res;
}

// END


