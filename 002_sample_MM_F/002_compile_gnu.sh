#!/bin/bash

	# 1. compile test

	g++  -static                    \
	     -isystem 			       \
          /opt/gtest/170/include     \
          sample_2.cpp               \
          sample_2_unittest.cpp      \
      	-pthread 			       \
          /opt/gtest/170/libgtest.a  \
          -o x_gnu

